import React, { Component } from 'react';
import './App.css';

import SequenceTab from './components/SequenceTab';
import ContentWrapper from './components/ContentWrapper';
import axios from 'axios';


class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      activeTab: 'fibonacci',
      currentValue: 0,
      tabs: [{
        name: 'fibonacci',
        active: true,
        url: '/api/numbers/fibonacci',
      }, {
        name: 'factorial',
        active: false,
        url: '/api/numbers/factorial',
      }]
    };
  }

  tabNavigation(name) {
    const tabs = this.state.tabs.map((tab) => {
      return { ...tab, active: tab.name === name };
    });

    this.setState({ tabs: tabs, activeTab: name });

    const { url } = tabs.find(({ active }) => active);

    axios.get(url).then(({ data: { result } }) => {
      this.setState({ currentValue: result });
    });

    return false;
  }

  nextValue() {
    this.tabNavigation(this.state.activeTab);
  }

  componentDidMount() {
    this.nextValue();
  }

  render() {
    return (
      <div className="App">
        <nav>
          {this.state.tabs.map(({ name, url, active }) => {
            return (<SequenceTab
                        key={name}
                        tabName={name}
                        url={url}
                        active={active}
                        onNavigation={(evt) => { evt.preventDefault(); this.tabNavigation(name); } } >
                    </SequenceTab>);
          })}
        </nav>
        <ContentWrapper value={ this.state.currentValue } onNextValue={() => this.nextValue()} />
      </div>
    );
  }
}

export default App;
